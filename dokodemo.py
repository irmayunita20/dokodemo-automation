from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.support.ui import Select

# Chrome Web Driver
options = webdriver.ChromeOptions()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options, executable_path=r'C:\Users\irmay\PycharmProjects\Login\Browsers\chromedriver.exe')

# Login
driver.get("http://dokodemo-kerja.l72.logique.co.id/login")
WebDriverWait(driver, 4).until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR,"iframe[name^='a-'][src^='https://www.google.com/recaptcha/api2/anchor?']")))
WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='recaptcha-anchor']"))).click()
driver.switch_to.default_content()
driver.find_element_by_name("email").send_keys("admin@admin.com")
time.sleep(3)
driver.find_element_by_name("password").send_keys("Admin12345")
time.sleep(3)
WebDriverWait(driver, 15).until(EC.element_to_be_clickable((By.XPATH, "//button[@type='submit']"))).click()

# Add Task
driver.get("http://dokodemo-kerja.l72.logique.co.id/task")
driver.find_element_by_xpath("/html/body/div[7]/div/form/div/div[3]/a/div[1]/div[2]").click()
driver.find_element_by_id("project_name").send_keys("Dokodemo")
select = Select(driver.find_element_by_id('departement_project'))
select.select_by_visible_text('Abc')
time.sleep(3)
driver.switch_to.default_content()
driver.find_element_by_xpath("/html/body/div[3]/div/div/div/div/form/div[4]/div[2]/button").click()

# Add Team
driver.get("http://dokodemo-kerja.l72.logique.co.id/team")
driver.find_element_by_xpath("/html/body/div[7]/div/form/div/div[3]/a/div[1]/div[2]").click()
driver.find_element_by_id("name").send_keys("Team Selenium1")
driver.find_element_by_xpath("/html/body/div[3]/div/div/div/div/form/div[2]/div[2]/button").click()

# Add Staff
driver.get("http://dokodemo-kerja.l72.logique.co.id/staff")
driver.find_element_by_xpath("/html/body/div[7]/div/form/div/div[3]/a/div[1]/div[2]").click()
driver.find_element_by_id("email").send_keys("irmayunita20691+61@gmail.com")
driver.find_element_by_id("fullname").send_keys("Irma 61")
select = Select(driver.find_element_by_id('privilege'))
select.select_by_visible_text('Staff')
time.sleep(3)
driver.switch_to.default_content()
select = Select(driver.find_element_by_id('departement'))
select.select_by_visible_text('121')
time.sleep(3)
driver.switch_to.default_content()
driver.find_element_by_xpath("/html/body/div[3]/div/div/div/div/form/div[6]/div[2]/button").click()



